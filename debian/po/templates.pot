# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the openstreetmap-carto package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: openstreetmap-carto\n"
"Report-Msgid-Bugs-To: openstreetmap-carto@packages.debian.org\n"
"POT-Creation-Date: 2015-10-11 13:38+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../openstreetmap-carto-common.templates:2001
msgid "Download OpenStreetMap data files from the Internet?"
msgstr ""

#. Type: boolean
#. Description
#: ../openstreetmap-carto-common.templates:2001
msgid ""
"The openstreetmap-carto stylesheet uses several data files that must be "
"downloaded from the Internet."
msgstr ""

#. Type: boolean
#. Description
#: ../openstreetmap-carto-common.templates:2001
msgid ""
"If you choose not to do this now, it can be done manually later by running "
"the \"get-external-data.py\" script in the /usr/share/openstreetmap-carto-"
"common directory."
msgstr ""

#. Type: string
#. Description
#: ../openstreetmap-carto.templates:2001
msgid "PostgreSQL database name:"
msgstr ""

#. Type: string
#. Description
#: ../openstreetmap-carto.templates:2001
msgid ""
"The openstreetmap-carto stylesheet uses a PostgreSQL database to store "
"OpenStreetMap data."
msgstr ""

#. Type: string
#. Description
#: ../openstreetmap-carto.templates:2001
msgid "Please choose the name for this database."
msgstr ""
